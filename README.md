# Valentin-Verschlüsselung

Es sollen in Bildern versteckte Nachrichten ausgelesen werden. Ein Pixel eines Bildes enthält 3 Werte zwischen 0 und 255 für die RGB-Farbcodierung. Diese soll verwendet werden, um Texte zu verstecken. Der R-Wert des Pixels in der linken oberen Ecke enthält den Ascii-Code des ersten Buchstabens der Nachricht. Der G-Wert gibt an, wie viele Pixel weiter rechts und der B-Wert wie viele Pixel weiter unten der  nächste relevante Pixel zu finden ist. So kann Pixel für Pixel, Buchstabe für Buchstabe die Nachricht entschlüsselt werden. Ist der G-Wert und der B-Wert 0, dann ist die Nachricht zu Ende. 

![](img/valentin.png)

Läuft die Suche über den rechten oder unteren Bildrand hinaus, wird wieder links in der gleichen Zeile bzw oben in der gleichen Spalte weiter gesucht. 

Entwickeln Sie ein objektorientiertes Programm, das laut dem oben beschriebenen Verfahren die Nachrichten aus den Bildern entschlüsselt und ausgibt. 

Optional: Verstecken Sie möglichst unauffällig Nachrichten in Bildern nach dem gleichen Prinzip.